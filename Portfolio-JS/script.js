let tablinks = document.getElementsByClassName("tab-links");
let tabcontents = document.getElementsByClassName("tab-contents");

// Skills Education Experience show
function opentab(tabname) {
  for (tablink of tablinks) {
    tablink.classList.remove("active-link");
  }
  for (tabcontent of tabcontents) {
    tabcontent.classList.remove("active-tab");
  }

  event.currentTarget.classList.add("active-link");
  document.getElementById(tabname).classList.add("active-tab");
}

// ---------- Hide or open Menu ----------

let sideMenu = document.getElementById("sidemenu");

function openMenu() {
  sideMenu.style.right = "0";
}

function closeMenu() {
  sideMenu.style.right = "-200px";
}

// Google Sheet Working form sctipt

const scriptURL =
  "https://script.google.com/macros/s/AKfycbxW90_6zCC-2WMsiGioeCBr0pCAmqe_0gREuINjmiHLwG6p8Kx6eUxJ1wb8ZAIdCG5LEQ/exec";
const form = document.forms["submit-to-google-sheet"];
const msg = document.getElementById('msg');                   //For showing the message of form submited/message has sent successfully

form.addEventListener("submit", (e) => {
  e.preventDefault();
  fetch(scriptURL, { method: "POST", body: new FormData(form) })
    .then((response) => {
        msg.innerHTML = "The Message has sent to Raviraj Successfully :)"
        setTimeout(function(){
            msg.innerHTML = ""
        }, 2000)
        form.reset()
    })
    .catch((error) => console.error("Error!", error.message));
});







// ----------------------------------------------------------------------------------------------------------------------------------------------------------------
// You can use this way also but for less syntax we will use loops and predefined functions And to use this you need to give ID to all the tab-titles

// let skills = document.getElementById('skills');
// let education = document.getElementById('Education');
// let experience = document.getElementById('Experience');

// let allSkills = document.getElementById('all-skills');
// let allEducation = document.getElementById('all-education');
// let allExperience = document.getElementById('all-experience');

// skills.onclick = function(){
//     allSkills.classList.add('active-tab');
//     allEducation.classList.remove('active-tab');
//     allExperience.classList.remove('active-tab');

//     skills.classList.add('active-link');
//     education.classList.remove('active-link');
//     experience.classList.remove('active-link');
// }

// education.onclick = function(){
//     allEducation.classList.add('active-tab');
//     allSkills.classList.remove('active-tab');
//     allExperience.classList.remove('active-tab');

//     skills.classList.remove('active-link');
//     education.classList.add('active-link');
//     experience.classList.remove('active-link');
// }

// experience.onclick = function(){
//     allExperience.classList.add('active-tab');
//     allSkills.classList.remove('active-tab');
//     allEducation.classList.remove('active-tab');

//     skills.classList.remove('active-link');
//     education.classList.remove('active-link');
//     experience.classList.add('active-link');
// }
